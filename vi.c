/*
 * vee-eye: Jody Bruchon's clone of 'vi'
 * Copyright (C) 2015-2024 by Jody Bruchon <jody@jodybruchon.com>
 * Distributed under the MIT License (see LICENSE for details)
 *
 * This clone of 'vi' works with a doubly linked list of text lines.
 * While it may be a bad idea in certain rare circumstances, the use
 * of individual lines as the basic unit of data makes a lot of work
 * in the code easier. The cursor is the "focal point" in this vi;
 * everything is calculated relative to the cursor and the window.
 * Lines longer than the terminal width are handled using a global
 * "line shift" that pushes the screen to the right.
 *
 * I created it mainly as a challenge to learn and improve my skills
 * in C. It is not a very serious project, but if it is useful to
 * you, please let me know! I'll be happy to hear about it. :-)
 */

#define VER "0.4"
#define VERDATE "2024-01-14"

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#ifndef NO_SIGNALS
 #include <signal.h>
#endif	/* NO_SIGNALS */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <termios.h>


/* Dev86 used for ELKS isn't C99 compliant */
#ifdef __ELKS__
 #define uintptr_t unsigned short
 #define restrict
 #define inline
 #define PATH_MAX 256
#else
 #include <stdint.h>
#endif	/* __ELKS__ */

/* ANSI character format sequences */
#define ANSI_RESET "\033[0m"
#define ANSI_BOLD "\033[1m"
#define ANSI_DIM "\033[2m"
#define ANSI_ITALIC "\033[3m"
#define ANSI_UNDER "\033[4m"
#define ANSI_BLINK "\033[5m"
#define ANSI_BOLD "\033[1m"
#define ANSI_REV "\033[7m"
#define ANSI_STRIKE "\033[9m"
#define COLOR_BLK "\033[30m"
#define COLOR_RED "\033[31m"
#define COLOR_GRN "\033[32m"
#define COLOR_YEL "\033[33m"
#define COLOR_BLU "\033[34m"
#define COLOR_MAG "\033[35m"
#define COLOR_CYA "\033[36m"
#define COLOR_WHT "\033[37m"
#define BG_BLK "\033[40m"
#define BG_RED "\033[41m"
#define BG_GRN "\033[42m"
#define BG_YEL "\033[43m"
#define BG_BLU "\033[44m"
#define BG_MAG "\033[45m"
#define BG_CYA "\033[46m"
#define BG_WHT "\033[47m"
#define SET_FORMAT(a) write(STDOUT_FILENO, a, sizeof(a) - 1)

/* Microsecond delay for ANSI escape sequence inputs */
#define ESC_DELAY 7000


/* Text is stored line-by-line. Line lengths are stored with the text data
 * to avoid lots of strlen() calls for line wrapping, insertion, etc.
 * alloc_size is the total allocated size of the text[] element. This
 * program always allocates more space than is required for each line so
 * that subsequent edit operations minimize allocations. */
struct line {
	struct line *prev;
	struct line *next;
	char *text;
	int len;
	int alloc_size;
	int tabs;
};
static struct line *line_head = NULL;


/* X/Y cursor coordinates */
struct coord {
	int x;
	int y;
};
static struct coord crsr;


/* Terminal configuration data */
static struct termios term_orig, term_config;
static int termdesc = -1;
static int term_rows;
static int term_real_rows;
static int term_cols;


/* Current editing locations (screen and file) */
static int cur_line, line_shift;
#define MAX_CRSR_SETSTRING 16
static char crsr_set_string[MAX_CRSR_SETSTRING];

/* Current file name */
static char curfile[PATH_MAX];

/* Track current line's struct pointer to avoid extra walks */
static struct line *cur_line_s = NULL;

/* Maximum size of command mode commands and status - MUST BE THE SAME */
#define MAX_CMDSIZE 127
#define MAX_STATUS 128

/* Current mode: 0=command, 1=insert, 2=replace */
#define MODE_COMMAND 0
#define MODE_INSERT 1
#define MODE_REPLACE 2
static int vi_mode = 0;
static const char * const mode_string[] = {
	"", "-- INSERT --", "-- REPLACE --"
};
static char custom_status[MAX_STATUS] = "";

/* Total number of lines allocated */
static int line_count = 0;

/* Buffer has been modified */
static int modified = 0;

/* Tab stop size and tabbed line */
static int tab_stop = 8;
static int tab_line_alloc = 0;
static int tab_line_len;
static char *tab_line = NULL;

/* File read/write buffer */
#define CHUNK_SIZE 4096
static char buf[CHUNK_SIZE];

/* Cursor/screen control macros and strings */
static const char seq_dlwrap[] = "\033[7l";
static const char seq_elwrap[] = "\033[7h";
static const char seq_scroll_up[] = "\033M";
static const char seq_scroll_down[] = "\033D";
static const char seq_clear_screen[] = "\033[H\033[J";
static const char seq_erase_line[] = "\033[2K";
static const char seq_erase_to_eol[] = "\033[K";
//static const char seq_crsr_home[] = "\033[H";
//static const char seq_crsr_up[] = "\033[1A";
//static const char seq_crsr_down[] = "\033[1B";
static const char seq_crsr_left[] = "\033[1D";
static const char seq_crsr_right[] = "\033[1C";

#define DISABLE_LINE_WRAP()	write(STDOUT_FILENO, seq_dlwrap, 4)
#define ENABLE_LINE_WRAP() 	write(STDOUT_FILENO, seq_elwrap, 4)
#define CRSR_RESTORE()	crsr_yx(crsr.y, crsr.x)
#define SCROLL_UP()	crsr_yx(1,1); write(STDOUT_FILENO, seq_scroll_up, 2); redraw_screen(0, 0)
#define SCROLL_DOWN()	crsr_yx(term_real_rows, 1); write(STDOUT_FILENO, seq_scroll_down, 2); redraw_line(cur_line_s, crsr.y)
#define CLEAR_SCREEN()	write(STDOUT_FILENO, seq_clear_screen, 6)
#define ERASE_LINE()	write(STDOUT_FILENO, seq_erase_line, 4)
#define ERASE_TO_EOL()	write(STDOUT_FILENO, seq_erase_to_eol, 3)
#define CRSR_HOME()	write(STDOUT_FILENO, seq_crsr_home, 3)
#define CRSR_UP()	write(STDOUT_FILENO, seq_crsr_up, 4)
#define CRSR_DOWN()	write(STDOUT_FILENO, seq_crsr_down, 4)
#define CRSR_LEFT()	write(STDOUT_FILENO, seq_crsr_left, 4)
#define CRSR_RIGHT()	write(STDOUT_FILENO, seq_crsr_right, 4)

#define CONTROL(a) (a - 0x40)


/* Function prototypes */
void oom(void);
static int destroy_line(struct line *target_line);
static void *alloc_text_chunk(int len, int *size);
static struct line *alloc_new_line(int start, const char * const restrict new_text, struct line **buf_head);
static int find_string(char *string, int forward);
static char *get_escape_string(void);
static int join_next_line(int start, int space);
static int jump_to_line(int line);
static void jump_page(int half, int down);
static int make_tabbed_line(struct line *line);
static void read_term_dimensions(void);
static void clean_abort(void);
static void file_status(void);
static void update_status(void);
static void redraw_screen(int row_start, int row_end);
static void redraw_line(struct line *line, int y);
static void destroy_buffer(struct line **head);
static void do_case_toggle_under_crsr(void);
static int do_del_under_crsr(int left);
static void do_cursor_up(void);
static void do_cursor_down(void);
static void do_cursor_left(void);
static void do_cursor_right(void);
static void move_to_line_start(void);
static void move_to_line_end(void);
static void move_to_prev_word(void);
static void move_to_next_word(void);


/***************************************************************************/


static void version_status(void)
{
	snprintf(custom_status, MAX_STATUS,
			ANSI_BOLD COLOR_CYA "vee-eye "
			COLOR_WHT "%s (%s) "
			COLOR_GRN "by Jody Bruchon "
			COLOR_RED "<jody@jodybruchon.com>"
			ANSI_RESET, VER, VERDATE);
	return;
}


static void alloc_initial_line(void)
{
	cur_line_s = alloc_new_line(line_count, NULL, &line_head);
	if (!cur_line_s) {
		fprintf(stderr, "Cannot create initial line\n");
		clean_abort();
	}
}


/* Cursor control functions */
void crsr_yx(int row, int col)
{
	snprintf(crsr_set_string, MAX_CRSR_SETSTRING, "\033[%d;%df", row, col);
	write(STDOUT_FILENO, crsr_set_string, strlen(crsr_set_string));
}


static void set_scroll_area(void) {
	snprintf(crsr_set_string, MAX_CRSR_SETSTRING, "\033[%d;%dr", 1, term_rows);
	write(STDOUT_FILENO, crsr_set_string, strlen(crsr_set_string));
}


#ifndef NO_SIGNALS
/* Window size change handler */
void sigwinch_handler(int signum, siginfo_t *sig, void *context)
{
	read_term_dimensions();
	set_scroll_area();
	if (crsr.x >= term_cols) crsr.x = term_cols - 1;
	if (crsr.y >= term_rows) crsr.y = term_rows - 1;
	move_to_line_start();
	redraw_screen(0, 0);
	snprintf(custom_status, MAX_STATUS, COLOR_YEL "Terminal resized to " COLOR_WHT "%dx%d" ANSI_RESET, term_cols, term_rows + 1);
	return;
}
#endif	/* NO_SIGNALS */


/* Read terminal dimensions */
static void read_term_dimensions(void)
{
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	term_real_rows = w.ws_row;
	term_rows = w.ws_row - 1;
	term_cols = w.ws_col;

	/* Prevent dimensions from being too small */
	if (term_real_rows < 1) term_real_rows = 1;
	if (term_rows < 1) term_rows = 1;
	if (term_cols < 1) term_cols = 1;

	return;
}


/* Make a temporary line copy with tabs converted to spaces */
static int make_tabbed_line(struct line *line)
{
	int maxlen = line->len + (line->tabs * tab_stop) + 1;
	char *orig, *tabbed;
	int tab_left;

	/* If no tabs are present then don't do this */
	if (line->tabs == 0) return 0;

	if (tab_line_alloc < maxlen) {
		if (tab_line != NULL) free(tab_line);
		tab_line = (char *)malloc(maxlen);
		if (tab_line == NULL) oom();
		tab_line_alloc = maxlen;
	}

	orig = line->text;
	tabbed = tab_line;
	tab_left = tab_stop;
	while (*orig != '\0') {
		if (*orig == '\t') {
			memset(tabbed, ' ', tab_left);
			tabbed += tab_left;
			tab_left = tab_stop;
		} else {
			*tabbed = *orig;
			tabbed++;
			tab_left--;
			if (tab_left == 0) tab_left = tab_stop;
		}
		orig++;
	}
	*tabbed = '\0';

	tab_line_len = (int)(tabbed - tab_line);
	return tab_line_len;
}


/* Write a line to the screen with appropriate shift */
static void redraw_line(struct line *line, int y)
{
	char *p;
	int len;

	if (!line || !line->text) goto error_null;

	len = make_tabbed_line(line);
	if (len != 0) {
		p = tab_line;
	} else {
		p = line->text + line_shift;
		len = line->len - line_shift;
	}
	sprintf(crsr_set_string, "\033[%d;1f" ANSI_RESET, y);
	write(STDOUT_FILENO, crsr_set_string, strlen(crsr_set_string));
	if (len > term_cols) len = term_cols;
	if (len > 0) write(STDOUT_FILENO, p, len);
	crsr_yx(y, len + 1);
	if (len < term_cols) ERASE_TO_EOL();
	CRSR_RESTORE();
	//write(STDOUT_FILENO, "\n", 1);
	//sleep(1);
	return;

error_null:
	fprintf(stderr, "error: redraw_line psased a NULL pointer\n");
	clean_abort();
}


/* Reduce line shift and redraw entire screen */
static void line_shift_reduce(int count)
{
	if (line_shift <= 0) return;

	if (count == 0 || count >= line_shift) line_shift = 0;
	else line_shift -= count;
	redraw_screen(0, 0);
	return;
}


/* Increase line shift and redraw entire screen */
static void line_shift_increase(int count)
{
	line_shift += count;
	redraw_screen(0, 0);
	return;
}


/* Walk the specified line list to the requested line */
static struct line *walk_to_line(int num, struct line *line)
{

	if (num == 0) return line;

	for (int i = 1; line != NULL; line = line->next, i++) {
		if (i == num) break;
		if (line->next == NULL) return NULL;
	}
	return line;
}


static int jump_to_line(int line)
{
	int mid;

	if (line > line_count || line < 1) return -1;

	cur_line = line;
	cur_line_s = walk_to_line(line, line_head);
	crsr.x = 1;
	line_shift = 0;

	mid = term_real_rows / 2;

	crsr.y = mid;
	if (mid > line) crsr.y = line;
	if (line_count < term_real_rows) crsr.y = line;

	redraw_screen(0, 0);

	return 0;
}


/* Skip half or whole pages, down or up */
static void jump_page(int half, int down)
{
	int jump;

	half++;
	if (down != 0) {
		jump = cur_line + (term_real_rows / half);
		if (jump > line_count) jump = line_count;
	} else {
		jump = cur_line - (term_real_rows / half);
		if (jump < 1) jump = 1;
	}
	jump_to_line(jump);
	return;
}


/* Out of memory */
void oom(void) {
	strcpy(custom_status, "out of memory");
	update_status();
	clean_abort();
}


/* Allocate in 32-byte chunks: add null terminator room then
 * shift-divide; increment and shift-multiply to get the
 * number of bytes we need to allocate. */
static void *alloc_text_chunk(int len, int *size)
{
	int alloc_size;

	alloc_size = ((len + 1) >> 5) + 1;
	alloc_size <<= 5;
	*size = alloc_size;
	return calloc(1, alloc_size);
}


/* Allocate a new line after the selected line */
static struct line *alloc_new_line(int start, const char * const restrict new_text, struct line **buf_head)
{
	struct line *prev_line, *new_line;

	/* Cannot open lines out of current range */
	if (start > line_count) return NULL;

	prev_line = walk_to_line(start, *buf_head);

	/* Insert a new line */
	new_line = (struct line *)calloc(1, sizeof(struct line));
	if (!new_line) oom();
	if (prev_line == NULL) {
		/* If buf_head is NULL, no lines exist yet */
		*buf_head = new_line;
		new_line->next = NULL;
		new_line->prev = NULL;
	} else {
		/* Insert this line after the existing one */
		new_line->next = prev_line->next;
		new_line->prev = prev_line;
		prev_line->next = new_line;
		if (new_line->next != NULL)
			new_line->next->prev = new_line;
	}

	/* If inserting between two lines, link the next one to us */
	if (new_line->next != NULL) new_line->next->prev = new_line;

	/* Allocate the text area (if applicable) */
	if (new_text == NULL) {
		new_line->len = 0;
		new_line->text = (char *)calloc(1, 32);
		if (!new_line->text) oom();
		new_line->alloc_size = 32;
	} else {
		new_line->len = strlen(new_text);
		new_line->text = alloc_text_chunk(new_line->len, &(new_line->alloc_size));
		if (!new_line->text) oom();
		strcpy(new_line->text, new_text);
	}

	line_count++;

	return new_line;
}


/* Search with / and ? */
static int find_string(char *string, int forward)
{
	int line, len, pos;
	char sc;
	struct line *line_s;

	len = strlen(string);
	line = cur_line;
	pos = crsr.x + line_shift;
	line_s = walk_to_line(line, line_head);

	if (forward == 0) {
		sc = *(string + len - 1);
		while (line > 0) {
			while (pos >= len) {
				if (*(line_s->text + pos) == sc) {
					if (strncmp((line_s->text + pos + 1 - len), string, len) == 0) {
						pos = pos + 1 - len;
						goto found_string;
					}
				} else pos--;
			}
			line_s = line_s->prev;
			pos = line_s->len;
		}
	} else {
		sc = *string;
		while (line <= line_count) {
			while (pos <= line_s->len + 1 - len) {
				if (*(line_s->text + pos) == sc) {
					if (strncmp((line_s->text + pos), string, len) == 0) {
						goto found_string;
					}
				} else pos++;
			}
			line_s = line_s->next;
			pos = line_s->len;
		}
	}
	/* Failed to find the string */
	return -1;

found_string:
	crsr.x = crsr.x; // FIXME: set screen position properly here
	redraw_screen(0, 0);
	return 0;
}


static char *get_escape_string(void)
{
	char *es;
	unsigned char c;
	int len = 0;
	int cntl;

	es = (char *)malloc(MAX_CMDSIZE);
	if (es == NULL) oom();
	cntl = fcntl(STDIN_FILENO, F_GETFL);
	fcntl(STDIN_FILENO, F_SETFL, cntl | O_NONBLOCK);

	usleep(ESC_DELAY);
	while (read(STDIN_FILENO, &c, 1) > 0) {
		*(es + len) = c;
		len++;
		if (len >= MAX_CMDSIZE - 1) break;
		usleep(ESC_DELAY);
	}

	*(es + len) = '\0';
	fcntl(STDIN_FILENO, F_SETFL, cntl);

	return es;
}


/* Handle ANSI escape sequences
   Return: 0 = OK, 1 = empty, 2 = DEL */
static int handle_escape_sequence(void)
{
	int ret = 0;
	char *es;

	es = get_escape_string();
	/* Lone ESC; cancel interpretation */
	if (*es == '\0') { ret = 1; goto end_escape; }
	if (strcmp(es, "[A") == 0) { do_cursor_up(); goto end_escape; }       /* Up Arrow */
	if (strcmp(es, "[B") == 0) { do_cursor_down(); goto end_escape; }     /* Down Arrow */
	if (strcmp(es, "[C") == 0) { do_cursor_right(); goto end_escape; }    /* Right Arrow */
	if (strcmp(es, "[D") == 0) { do_cursor_left(); goto end_escape; }     /* Left Arrow */
	if (strcmp(es, "[3~") == 0) { ret = 2; goto end_escape; }             /* DEL */
	if (strcmp(es, "[5~") == 0) { jump_page(0, 0); goto end_escape; }     /* Page Up */
	if (strcmp(es, "[6~") == 0) { jump_page(0, 1); goto end_escape; }     /* Page Down */
	if (strcmp(es, "[F") == 0) { move_to_line_end(); goto end_escape; }   /* End */
	if (strcmp(es, "[H") == 0) { move_to_line_start(); goto end_escape; } /* Home */

end_escape:
	CRSR_RESTORE();
	free(es);
	return ret;
}


/* 'J' command: join next line to end of this line */
static int join_next_line(int start, int space)
{
	struct line *cur_line;
	char *new_text, *skip;
	int new_len, new_alloc;

	/* Refuse invalid joins */
	if (start > line_count) return -1;
	if (start == line_count) return 0;

	if (start > 0) cur_line = walk_to_line(start, line_head);
	else cur_line = line_head;
	if (cur_line == NULL || cur_line->next == NULL || cur_line->text == NULL || cur_line->next->text == NULL) return 0;

	new_len = strlen(cur_line->text) + strlen(cur_line->next->text) + 2;
	new_text = alloc_text_chunk(new_len, &new_alloc);
	if (!new_text) oom();

	/* Assemble new line, skipping space/control chars in second line */
	strncpy(new_text, cur_line->text, new_alloc);
	if (space != 0) strncat(new_text, " ", new_alloc);
	skip = cur_line->next->text;
	while (*skip < 33 && *skip != '\0') skip++;
	strncat(new_text, skip, new_alloc);

	/* Free old stuff and replace with new merged line */
	free(cur_line->text);
	cur_line->len = new_len;
	cur_line->text = new_text;
	cur_line->alloc_size = new_alloc;
	destroy_line(cur_line->next);

	return 1;
}


/* Destroy and free one line in the line list */
static int destroy_line(struct line *target_line)
{
	struct line *temp_line;

	if (target_line == NULL) return -1;
	if (target_line->prev != NULL) {
		if (target_line->text != NULL) free(target_line->text);
		/* Detach the line to be destroyed from the list */
		target_line->prev->next = target_line->next;
		if (target_line->next != NULL)
			target_line->next->prev = target_line->prev;
		/* Jump to the next line and destroy the previous one */
		free(target_line);
		line_count--;
	} else {
		/* Line 1 must be handled differently */
		if (line_count > 1) {
			if (target_line->next == NULL) goto error_line_null;
			temp_line = target_line;
			target_line = target_line->next;
			target_line->prev = NULL;
			if (temp_line->text != NULL) free(temp_line->text);
			free(temp_line);
			/* Update line_head if we just destroyed it */
			if ((uintptr_t)temp_line == (uintptr_t)line_head)
				line_head = target_line;
			line_count--;
		} else {
			/* Line 1 with no more lines */
			target_line->len = 0;
			/* Warn if user tried to delete the only empty line */
			if (*target_line->text == '\0') return 1;
			*(target_line->text) = '\0';
		}
	}

	modified = 1;
	redraw_screen(0, 0);

	return 0;

error_line_null:
	fprintf(stderr, "error: line 1 next ptr = NULL but line_count > 1\n");
	clean_abort();
	return -1;
}

static void file_status(void)
{
	snprintf(custom_status, MAX_STATUS, COLOR_WHT "%s: " COLOR_MAG "%smodified: " COLOR_YEL "line %d of %d [%d%%]",
			*curfile == '\0' ? "[new file]" : curfile,
			modified == 1 ? "un" : "",
			cur_line, line_count,
			(cur_line * 100) / line_count);
	return;
}


/* Destroy every line in the selected buffer
 * This is used to empty the yank buffer and to de-allocate the line buffer
 * on program exit; use it like this: destroy_buffer(&buffer_head); */
static void destroy_buffer(struct line **head)
{
	struct line *line = *head;
	struct line *prev = NULL;

	*head = NULL;

	/* Free lines in order until list is exhausted */
	while (line != NULL) {
		if (line->text != NULL) free(line->text);
		if (line->prev != NULL) free(line->prev);
		prev = line;
		line = line->next;
	}
	/* Free the final line, if applicable */
	if (prev != NULL) free(prev);
}

static void update_status(void)
{
	char num[4];

	/* Move the cursor to the last line */
	crsr_yx(term_real_rows, 0);
	ERASE_LINE();

	/* Print the current insert/replace mode or special status */
	if (*custom_status == '\0')
		strncpy(custom_status, mode_string[vi_mode], MAX_STATUS - 1);
	SET_FORMAT(COLOR_CYA);
	write(STDOUT_FILENO, custom_status, strlen(custom_status));
	SET_FORMAT(ANSI_RESET);
	//memset(custom_status, 0, MAX_STATUS);
	*custom_status = '\0';

	/* Print our location in the current line and file */
	crsr_yx(term_real_rows, term_cols - 16);
	printf("%d,%d", cur_line, crsr.x + line_shift);
	crsr_yx(term_real_rows, term_cols - 5);
	if (cur_line == 1) write(STDOUT_FILENO, " Top", 4);
	else if (cur_line == line_count) write(STDOUT_FILENO, " Bot", 4);
	else {
		snprintf(num, 4, "%d%%", (cur_line * 100) / line_count);
		write(STDOUT_FILENO, num, strlen(num));
	}

	CRSR_RESTORE();

	return;
}


/* Redraw part or all of the screen */
static void redraw_screen(int row_start, int row_end)
{
	struct line *line;
	int start_y;
	int this_row;

	if (cur_line < crsr.y) goto error_line_cursor;
	if (row_start > term_rows) goto error_row_params;

	if (row_start <= 0) row_start = 1;
	if (row_end <= 0) row_end = term_rows;

	if (row_start == 1 && row_end == term_rows) CLEAR_SCREEN();

	/* Get start line number and pointer */
	start_y = cur_line - crsr.y + row_start;

	/* Find the first line to write to the screen */
	line = walk_to_line(start_y, line_head);
	if (!line) goto error_line_walk;

	/* Draw lines until no more are left */
	this_row = row_start;
	while (this_row <= row_end) {
		/* Write out this line data */
		redraw_line(line, this_row);
		this_row++;
		crsr_yx(this_row, 1);
		line = line->next;
		if (line == NULL) break;
	}

	/* Fill the rest of the screen with tildes */
	while (this_row <= row_end) {
		write(STDOUT_FILENO, ANSI_BOLD COLOR_MAG "~\n", 12);
		this_row++;
	}

	CRSR_RESTORE();

	return;

error_line_cursor:
	fprintf(stderr, "error: cur_line < crsr.y\n");
	clean_abort();
error_row_params:
	fprintf(stderr, "error: bad row params: %u, %u\n",
			row_start, row_end);
	clean_abort();
error_line_walk:
	fprintf(stderr, "error: line walk invalid (%d) (%d - %d)\n",
			start_y, cur_line, crsr.y);
	clean_abort();
}


/* Toggle case */
static void do_case_toggle_under_crsr(void)
{
	char *p = cur_line_s->text + crsr.x + line_shift - 1;

	if (*p >= 'a' && *p <= 'z') *p -= 32;
	else if (*p >= 'A' && *p <= 'Z') *p += 32;
	else return;
	modified = 1;
	redraw_line(cur_line_s, crsr.y);
	return;
}


/* Delete char at cursor location */
static int do_del_under_crsr(int left)
{
	char *p;

	if (cur_line_s->len == 0) return 1;
	if (crsr.x > (cur_line_s->len + line_shift) && left == 0) return 1;
	p = cur_line_s->text + crsr.x + line_shift;

	/* Copy everything down one char */
	memmove(p - 1, p, strlen(p) + 1);
	cur_line_s->len--;
	if (crsr.x > (cur_line_s->len - line_shift) + (vi_mode > 0 ? 1 : 0)) crsr.x--;
	if (crsr.x < 1) {
		crsr.x = 1;
		line_shift_reduce(1);
	} else redraw_line(cur_line_s, crsr.y);

	CRSR_RESTORE();
	modified = 1;
	return 0;
}


static void go_to_start_of_next_line(void)
{
	do_cursor_down();
	line_shift = 0;
	crsr.x = 1;
	CRSR_RESTORE();
	update_status();
	return;
}


/* Restore terminal to original configuration */
static void term_restore(void)
{
	if (termdesc != -1) tcsetattr(termdesc, TCSANOW, &term_orig);
	ENABLE_LINE_WRAP();
	return;
}

/* Initialize terminal settings */
static int term_init(void)
{
	/* Only init terminal once */
	if (termdesc != -1) return 0;

	/* Find a std* stream with a TTY */
	if (isatty(STDIN_FILENO)) termdesc = STDIN_FILENO;
	else if (isatty(STDOUT_FILENO)) termdesc = STDOUT_FILENO;
	else if (isatty(STDERR_FILENO)) termdesc = STDERR_FILENO;
	else return -ENOTTY;

	/* Get current terminal configuration and save it*/
	if (tcgetattr(termdesc, &term_orig)) return -EBADF;
	memcpy(&term_config, &term_orig, sizeof(struct termios));

	/* Disable buffering */
	if (isatty(STDIN_FILENO)) setvbuf(stdin, NULL, _IONBF, 0);
	if (isatty(STDOUT_FILENO)) setvbuf(stdout, NULL, _IONBF, 0);
	if (isatty(STDERR_FILENO)) setvbuf(stderr, NULL, _IONBF, 0);

	/* Configure terminal settings */

	/* c_cc */
	term_config.c_cc[VTIME] = 0;
	term_config.c_cc[VMIN] = 1;	/* read() one char at a time*/

	/* iflag */
#ifdef IUCLC
	term_config.c_iflag &= ~IUCLC;
#endif
	term_config.c_iflag &= ~PARMRK;
	term_config.c_iflag |= IGNPAR;
	term_config.c_iflag &= ~IGNBRK;
	term_config.c_iflag |= BRKINT;
	term_config.c_iflag &= ~ISTRIP;
	term_config.c_iflag &= ~(INLCR | IGNCR | ICRNL);

	/* cflag */
	term_config.c_cflag &= ~CSIZE;
	term_config.c_cflag |= CS8;
	term_config.c_cflag |= CREAD;

	/* lflag */
	term_config.c_lflag |= ISIG;
	term_config.c_lflag &= ~ICANON;	/* disable line buffering */
	term_config.c_lflag &= ~IEXTEN;

	/* disable local echo */
	term_config.c_lflag &= ~(ECHO | ECHONL | ECHOE | ECHOK);

	/* Finalize settings */
	tcsetattr(termdesc, TCSANOW, &term_config);

	/* Disable automatic line wrapping */
	DISABLE_LINE_WRAP();

	set_scroll_area();

	return 0;
}

/* Clean abort */
static void clean_abort(void)
{
	term_restore();
	destroy_buffer(&line_head);
	exit(EXIT_FAILURE);
}


void insert_char(char c)
{
	char *new_text;
	char *p;

	modified = 1;

	switch (vi_mode) {
	case 1:	/* insert mode */
		if (cur_line_s->alloc_size == (cur_line_s->len)) {
			/* Allocate a larger buffer and insert to that */
			new_text = (char *)realloc(cur_line_s->text, cur_line_s->len << 1);
			if (!new_text) oom();
			cur_line_s->text = new_text;
			cur_line_s->alloc_size = cur_line_s->len << 1;
		}
		/* Move text up by one byte */
		p = cur_line_s->text + crsr.x + line_shift - 1;
		memmove(p + 1, p, strlen(p) + 1);
		*p = c;
		if (crsr.x > term_cols) line_shift_increase(1);
		else crsr.x++;
		cur_line_s->len++;
		return;

	case 2: /* replace mode */
		return;
	}

	return;
}


/* Editing mode. Doesn't return until ESC pressed. */
void edit_mode(void)
{
	int i;
	char *fragment;
	unsigned char c;

	while (read(STDIN_FILENO, &c, 1)) {
		switch (c) {
		case '\033':
			switch (handle_escape_sequence()) {
				case 1: goto end_edit_mode;
				case 2: do_del_under_crsr(0); break;
			}
			continue;

		case '\0':
			continue;

		case '\b':
		case 0x7f:
			if (crsr.x > 1) {
				crsr.x--;
				do_del_under_crsr(1);
				continue;
			}

			/* Backspace at start of line */
			if (crsr.x == 1 && cur_line > 1) {
				do_cursor_up();
				i = cur_line_s->len + 1;
				join_next_line(cur_line, 0);
				crsr.x = i;
				CRSR_RESTORE();
			}
			continue;

		case '\n':
		case '\r':	/* New line */
			fragment = cur_line_s->text + line_shift + crsr.x - 1;
			if (!alloc_new_line(cur_line, fragment, &line_head)) oom();

			/* New lines need to break the old line apart */
			if (*fragment != '\0') {
				cur_line_s->len = line_shift + crsr.x - 1;
				*fragment = '\0';
			}
			redraw_line(cur_line_s, crsr.y);
			go_to_start_of_next_line();
			redraw_screen(crsr.y, 0);
			continue;

		default:
			break;
		}

		/* Catch any invalid characters */
		if (c < 32 || c > 127) {
			snprintf(custom_status, MAX_STATUS, "Invalid char entered: %u", c);
			update_status();
			continue;
		}

		/* Insert character at cursor position */
		insert_char(c);
		redraw_line(cur_line_s, crsr.y);
		CRSR_RESTORE();
		update_status();
	}

end_edit_mode:
	do_cursor_left();
	vi_mode = MODE_COMMAND;
	update_status();
	return;
}


/* Get a free-form command string from the user */
static int get_command_string(char *command)
{
	int cmdsize = 0;
	char cc;

	while (read(STDIN_FILENO, &cc, 1)) {
		/* If user presses ESC, abort the command */
		if (cc == '\033') {
			free(get_escape_string());
			command[0] = '\0';
			return -1;
		}

		/* Backspace */
		if (cc == '\b' || cc == 0x7f) {
			command[cmdsize] = '\0';
			cmdsize--;
			if (cmdsize < 0) return 0;;
			write(STDOUT_FILENO, "\b \b", 3);
			continue;
		}

		/* Newline or carriage return */
		if (cc == '\n' || cc == '\r') {
			command[cmdsize] = '\0';
			break;
		}
		write(STDOUT_FILENO, &cc, 1);
		command[cmdsize] = cc;
		cmdsize++;
		if (cmdsize == MAX_CMDSIZE) break;
	}
	return cmdsize;
}


static void do_cursor_left(void)
{
	if (crsr.x == 1) {
		line_shift_reduce(1);
		return;
	} else CRSR_LEFT();
	crsr.x--;
	return;
}

static void do_cursor_right(void)
{
	int mod = 0;
	int pos = crsr.x + line_shift;

	if (vi_mode != 0) mod = 1;

	if (cur_line_s->tabs > 0) {
		/* Tabbed lines require jumps */
	} else if (pos < cur_line_s->len + mod) {
		if (crsr.x == term_cols) line_shift_increase(1);
		else {
			crsr.x++;
			CRSR_RIGHT();
		}
	}
	return;
}


static void do_cursor_up(void)
{
	int pos = crsr.x + line_shift;

	if (cur_line == 1 || cur_line_s->prev == NULL) return;
	cur_line_s = cur_line_s->prev;
	cur_line--;
	if (crsr.y > 1) crsr.y--;
	else { SCROLL_UP(); }
	/* Pull the cursor to EOL if it is too far over */
	if (pos > cur_line_s->len) move_to_line_end();

	CRSR_RESTORE();
	return;
}


static void do_cursor_down(void)
{
	int pos = crsr.x + line_shift;

	if (cur_line == line_count) return;
	if (cur_line_s->next == NULL) return;
	cur_line_s = cur_line_s->next;
	cur_line++;
	if (crsr.y < term_rows) crsr.y++;
	else { SCROLL_DOWN(); }
	/* Pull the cursor to EOL if it is too far over */
	if (pos > cur_line_s->len) move_to_line_end();

	CRSR_RESTORE();
	return;
}


static void move_to_line_start(void)
{
	if (crsr.x == 1 && line_shift == 0) return;

	crsr.x = 1;
	if (line_shift > 0) line_shift_reduce(0);
	else redraw_line(cur_line_s, crsr.y);
	CRSR_RESTORE();
	return;
}


static void move_to_line_end(void)
{
	if (cur_line_s->len <= term_cols) {
		crsr.x = cur_line_s->len;
		if (line_shift > 0) line_shift_reduce(0);
		else redraw_line(cur_line_s, crsr.y);
	} else {
		crsr.x = term_cols;
		line_shift_increase(cur_line_s->len - term_cols - line_shift);
	}
	if (crsr.x < 1) crsr.x = 1;
	CRSR_RESTORE();
	return;
}


static void move_to_prev_word(void)
{
	char *p = cur_line_s->text + crsr.x + line_shift - 1;
	while ((crsr.x + line_shift > 1) && (*p != ' ')) {
		do_cursor_left();
		p--;
	}
	while ((crsr.x + line_shift > 2) && (*(p - 1) != ' ')) {
		do_cursor_left();
		p--;
	}
	if (p == cur_line_s->text) return;
	return;
}

static void move_to_next_word(void)
{
	char *p = cur_line_s->text + crsr.x + line_shift - 1;

	while ((crsr.x + line_shift < cur_line_s->len - 1) && (*p != ' ')) {
		do_cursor_right();
		p++;
	}
	if (*(p + 1) == '\0') return;
	if (*(p + 1) != ' ') do_cursor_right();
	return;
}


/* Load a file into buffer starting at a particular line */
int load_file(const char * const restrict name, const int start_line)
{
	FILE *fp;
	struct line *cur_load_line;
	int load_line_count = 0;
	int buflen, tabs;

	/* start_line = 0 will load at the beginning of the buffer */
	if ((start_line != 0) && (start_line > line_count)) return -1;
	if (name[0] == '\0') {
		strcpy(custom_status, "No file specified");
		return -2;
	}

	fp = fopen(name, "rb");
	if (!fp) return -3;

	cur_load_line = walk_to_line(start_line, line_head);
	if (cur_load_line == NULL) cur_load_line = line_head;
	/* Read the file into the buffer line by line */
	while (feof(fp) == 0) {
		buf[0] = '\0';
		if (fgets(buf, CHUNK_SIZE, fp) == NULL) {
			fclose(fp);
			break;
		}
		if (ferror(fp) != 0) {
			fclose(fp);
			return -4;
		}

		/* Count length while looking for control chars */
		/* Replaces: buflen = strlen(buf) - 1; */
		tabs = 0;
		buflen = -1;
		for (char *pos = buf; *pos != '\0'; pos++, buflen++) {
			if (*pos == '\t') tabs++;
			if (*pos == '\n' || *pos == '\r') {
				*pos = '\0';
				break;
			}
		}
		if (buf[buflen] == '\n' || buf[buflen] == '\r') buf[buflen] = '\0';
		cur_load_line = alloc_new_line(0, buf, &cur_load_line); // fast way
		cur_load_line->tabs = tabs;
		if (cur_load_line == NULL) {
			fclose(fp);
			return -5;
		}
		if (line_head == NULL) {
			line_head = cur_load_line;
			line_head->prev = NULL;
		}
		load_line_count++;
	}

	return load_line_count;
}


/* Save the buffer to the file specified */
int save_file(const char * const restrict name)
{
	FILE *fp;
	struct line *line = line_head;

	if (!name || *name == '\0') return -1;
	if (modified == 0) return 0;

	fp = fopen(name, "w+b");
	if (!fp) return -1;
	while (line) {
		errno = 0;
		fwrite(line->text, line->len, 1, fp);
		if (ferror(fp)) break;
		fwrite("\n", 1, 1, fp);
		if (ferror(fp)) break;
		line = line->next;
	}
	fclose(fp);

	if (errno != 0) return -1;
	return 0;
}


/* Handle an incoming command */
int do_cmd(char c)
{
	char command[MAX_CMDSIZE];
	char *filename;
	int cmd_len = 1;
	int num_times = 1;
	int i;

	command[0] = c; command[1] = '\0';

	if (c == '\0') goto end_cmd;
	if (c == '\033') {
		switch (handle_escape_sequence()) {
			case 1: goto end_cmd;
			case 2: do_del_under_crsr(0); break;
		}
		goto end_cmd;
	}
//fprintf(stderr, "do_cmd: 0x%x, %d, %d\n", c, c >= '0', c <= '9');
	while (c >= '0' && c <= '9') {
		strncpy(custom_status, command, cmd_len);
		update_status();
		read(STDIN_FILENO, &c, 1);
		command[cmd_len] = c; cmd_len++;
		command[cmd_len] = '\0';
		if (cmd_len >= MAX_CMDSIZE - 2) break;
	}

	if (cmd_len > 1) {
		strncpy(custom_status, command, MAX_STATUS);
		update_status();
		c = command[cmd_len];
		command[cmd_len] = '\0';
		num_times = atoi(command);
		command[cmd_len] = c;
		if (num_times < 1) num_times = 1;
	}

	switch (command[cmd_len - 1]) {
	case '\b':
	case 0x7f:
	case 'h': do_cursor_left(); break;
	case CONTROL('J'):
	case 'j': do_cursor_down(); break;
	case 'k': do_cursor_up(); break;
	case 'l': do_cursor_right(); break;
	case 'b': move_to_prev_word(); break;
	case 'w': move_to_next_word(); break;
	case '^': move_to_line_start(); break;
	case '$': move_to_line_end(); break;
	case 'G': jump_to_line(line_count); break;

	case CONTROL('G'): file_status(); break;
	case CONTROL('L'): redraw_screen(0, 0); break;

	case CONTROL('F'): jump_page(0, 1); break;
	case CONTROL('D'): jump_page(1, 1); break;
	case CONTROL('B'): jump_page(0, 0); break;
	case CONTROL('U'): jump_page(1, 0); break;

	case '~':
		do_case_toggle_under_crsr();
		do_cursor_right();
		break;

	case 'A':	/* append to end of line */
		move_to_line_end();
	case 'a':	/* append insert */
		vi_mode = MODE_INSERT;
		do_cursor_right();
		update_status();
		edit_mode();
		break;

	case 'I':	/* insert at start of line */
		move_to_line_start();
	case 'i':	/* insert */
		vi_mode = MODE_INSERT;
		update_status();
		edit_mode();
		break;

	case 'J':	/* join next line to this line with a space */
		join_next_line(cur_line, 1);
		break;

	case 'o':
		if (alloc_new_line(cur_line, NULL, &line_head) == NULL) oom();
		go_to_start_of_next_line();
		redraw_screen(crsr.y, 0);
		vi_mode = MODE_INSERT;
		update_status();
		edit_mode();
		break;

	case 'p':
		snprintf(custom_status, MAX_STATUS, "'put' command not yet supported");
		break;

	case 'x':	/* Delete char at cursor */
		for (i = num_times; i > 0; i--) {
			if (do_del_under_crsr(0) == 1) break;
		}
		break;

	case 'X':	/* Delete char left of cursor */
		for (i = num_times; i > 0; i--) {
			if (crsr.x == 1) break;
			crsr.x--;
			if (do_del_under_crsr(1) == 1) break;
		}
		break;

	case 'Z':
		read(STDIN_FILENO, &c, 1);
		if (c != 'Z') goto end_cmd;
		goto save_current_file;


#ifndef __ELKS__
	case '#':	/* NON-STANDARD cursor pos dump */
		redraw_screen(0, 0);
		snprintf(custom_status, MAX_STATUS,
				"win %dx%d  crsr %d+%d,%d  line %d/%d  len %d:%d",
				term_cols, term_real_rows, crsr.x, line_shift, crsr.y,
				cur_line, line_count, cur_line_s->len,
				cur_line_s->alloc_size);
		break;
#endif	/* __ELKS__ */

	case 'd':
		/* TODO: Replace with yank + delete */
		read(STDIN_FILENO, &c, 1);
		if (c == '\033') goto end_cmd;
		if (c == 'd') {
			for (i = num_times; i > 0; i--) {
				if (cur_line == line_count) {
					/* Last/only line is a special case */
					if (cur_line > 1) {
						do_cursor_up();
						destroy_line(cur_line_s->next);
						break;
					} else {
						destroy_line(cur_line_s);
						crsr.x = 0; line_shift = 0;
						CRSR_RESTORE();
						break;
					}
				} else {
					cur_line_s = cur_line_s->next;
					destroy_line(cur_line_s->prev);
				}
			}
			if (i < num_times) snprintf(custom_status, MAX_STATUS, "Deleted %d lines at %d",
					num_times - i, cur_line);
		}
		break;

#if 0
	case '?':
		crsr_yx(term_real_rows, 1);
		ERASE_LINE();
		write(STDOUT_FILENO, "?", 1);
		cmd_len = get_command_string(command);
		if (!cmd_len) break;

		i = find_backward(command);
		snprintf(custom_status, MAX_STATUS, "search commands not yet supported");
		goto end_cmd;

	case '/':
		crsr_yx(term_real_rows, 1);
		ERASE_LINE();
		write(STDOUT_FILENO, "/", 1);
		cmd_len = get_command_string(command);
		if (!cmd_len) break;

		i = find_forward(command);
		snprintf(custom_status, MAX_STATUS, "search commands not yet supported");
		goto end_cmd;

#endif

	case ':':	/* Colon command */
		crsr_yx(term_real_rows, 1);
		ERASE_LINE();
		write(STDOUT_FILENO, ":", 1);
		cmd_len = get_command_string(command);
		if (cmd_len < 1 || *command == '\0') goto end_cmd;

		if (strncmp(command, "ve", 2) == 0) {
			version_status();
			goto end_cmd;
		}

		if (strncmp(command, "wq", 2) == 0) {
save_current_file:
			/* Save to current file */
			if (save_file(curfile) == 0) goto end_vi;
			else snprintf(custom_status, MAX_STATUS, "Error saving file");
			goto end_cmd;
		}

		if (*command == 'r') {
			/* Load the file specified */
			filename = command + 2;
			i = -1;
			if (cmd_len >= 4 && *filename != '\0') i = load_file(filename, cur_line);
			if (i < 0) snprintf(custom_status, MAX_STATUS, "Error reading file '%s' [%d]", filename, i);
			else modified = 1;
			redraw_screen(crsr.y, 0);
			goto end_cmd;
		}

		if (*command == 'w') {
			/* Save the file specified */
			filename = command + 2;
			i = 0;
			if (cmd_len < 4 || *filename == '\0') {
				if (*curfile != 0) i = save_file(curfile);
				else snprintf(custom_status, MAX_STATUS, "Cannot save: no file name specified");
			} else i = save_file(filename);
			if (i) snprintf(custom_status, MAX_STATUS, "Error saving file");
			else modified = 0;  /* Reset modified status after saving */
			goto end_cmd;
		}

		if (strcmp(command, "q") == 0) {
			if (modified == 0) goto end_vi;
			snprintf(custom_status, MAX_STATUS, "Error: changes not saved (use :q! to force exit)");
			goto end_cmd;
		}

		if (strcmp(command, "q!") == 0) goto end_vi;

		if (*command >= '0' && *command <= '9') {
			/* Jump to line number */
			num_times = atoi(command);
			if (num_times > line_count) num_times = line_count;
			else if (num_times < 1) {
				snprintf(custom_status, MAX_STATUS, "Error: invalid line number given");
				goto end_cmd;
			}
			jump_to_line(num_times);
			goto end_cmd;
		}
		break;

	default:
		if (command[cmd_len - 1] >= 32 && command[cmd_len - 1] < 128)
			snprintf(custom_status, MAX_STATUS, "Unknown key %u [%c]", command[cmd_len - 1], command[cmd_len - 1]);
		else
			snprintf(custom_status, MAX_STATUS, "Unknown key %u", command[cmd_len - 1]);
		break;
	}

	/* Any command can jump here to finish up, including status updates */
end_cmd:
	CRSR_RESTORE();
	update_status();
	return 0;

end_vi:
	crsr_yx(term_real_rows, 1);
	ERASE_LINE();
	term_restore();
	destroy_buffer(&line_head);
	exit(EXIT_SUCCESS);
}


int main(int argc, char **argv)
{
	int i;
	char c;
#ifndef NO_SIGNALS
	struct sigaction act;

	/* Set up SIGWINCH handler for window resizing support */
	memset(&act, 0, sizeof(struct sigaction));
	sigemptyset(&act.sa_mask);
	act.sa_sigaction = sigwinch_handler;
	act.sa_flags = SA_SIGINFO;
	sigaction(SIGWINCH, &act, NULL);
#endif	/* NO_SIGNALS */

	/* Start an empty buffer or load a specified file */
	cur_line = 1;
	if (argc == 1) {
		*curfile = '\0';
		alloc_initial_line();
		version_status();
	} else {
		strncpy(curfile, argv[1], PATH_MAX - 1);
		i = load_file(curfile, 0);
		if (i == -3) {
			alloc_initial_line();
			snprintf(custom_status, MAX_STATUS, "'%s' [NEW FILE]", curfile);
		} else {
			if (i < 0) {
				fprintf(stderr, "Cannot load %s (error %d)\n", curfile, i);
				exit(EXIT_FAILURE);
			}
			cur_line_s = line_head;
			snprintf(custom_status, MAX_STATUS, COLOR_GRN "Read " COLOR_WHT "\"%s\" " COLOR_YEL "(%d lines)", curfile, i);
		}
	}

	/* Initialize the terminal - only exit with clean_abort() after this */
	if ((i = term_init()) != 0) {
		if (i == -ENOTTY) fprintf(stderr, "a tty is required\n");
		else fprintf(stderr, "cannot init terminal: %s\n", strerror(-i));
		clean_abort();
	}
	read_term_dimensions();
	CLEAR_SCREEN();

	/* Initialize the cursor position and draw the screen */
	crsr.x = 1; crsr.y = 1; line_shift = 0;
	redraw_screen(0, 0);
	update_status();

	/* Read commands forever */
	while (read(STDIN_FILENO, &c, 1)) do_cmd(c);
	clean_abort();
}
