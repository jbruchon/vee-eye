                    vee-eye: Jody Bruchon's clone of 'vi'
       Copyright (C) 2015-2024 by Jody Bruchon <jody@jodybruchon.com>
        Distributed under the MIT License (see LICENSE for details)

This clone of 'vi' works with a doubly linked list of text lines. While it
may be a bad idea in certain rare circumstances, the use of individual lines
as the basic unit of data makes a lot of work in the code easier. The cursor
is the "focal point" in this vi; everything is calculated relative to the
cursor and the window. Lines longer than the terminal width are handled using
a global "line shift" that pushes the screen to the right.

I created it mainly as a challenge to learn and improve my skills in C. It is
not a very serious project, but if it is useful to you, please let me know!
I'll be happy to hear about it. :-)


IMPLEMENTED COMMANDS:

Cursor control: h, j, k, l, b, w, ^, $, G, CTRL-[LFDBUJ]
Edit mode: A, a, I, i, o
Edit commands: ~, x, X, d, J
Other commands: ZZ, CTRL-G
Colon commands: r, w, wq, q, q!, ve[rsion], [linenum]
Special keys: arrow keys, PgUp/PgDn/Home/End, Delete
Non-standard: # dumps the current cursor position to the status line
