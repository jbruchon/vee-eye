vee-eye 0.4 (2024-01-14)

- Enable backspacing to previous line
- Add ANSI escape sequence input handler
- Support arrows, PgUp/PgDn/Home/End, and Delete keys
- Add CTRL-G command for file status/info
- Fix more scrolling bugs

vee-eye 0.3 (2024-01-11)

- Add jumping and movement commands

vee-eye 0.2 (2024-01-10)

- Lots of crash and scrolling fixes
- Add several basic editing commands
- Add color to status line
- Prevent exit without saving when buffer is modified

vee-eye 0.1 (2023-12-16)

- First release, pretty broken
